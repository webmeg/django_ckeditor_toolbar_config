# -*- coding: utf-8 -*-
__author__ = 'Evgeniy Tatarintsev'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Source', 'RemoveFormat', 'DocProps', 'Templates', 'PasteFromWord', '-', 'Undo', 'Redo'],
            ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'],
            ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'Blockquote'],
            ['Link', 'Unlink', 'Anchor'],
            ['Styles', 'Format', 'Font', 'FontSize'],
            ['TextColor', 'BGColor'],
            ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Maximize', 'ShowBlocks', '-', 'About'],
        ]
    }
}
